import UIKit
import CoreMotion

class GameViewController: UIViewController {
    
    //MARK: - Enums
    enum MoveDirection {
        case left
        case right
    }
    
    // MARK: - IBOutlets
    @IBOutlet weak var backGroundView: UIImageView!
    @IBOutlet weak var leftShoreView: UIImageView!
    @IBOutlet weak var rightShoreView: UIImageView!
    
    // MARK: - let
    private let boatView = UIImageView()
    private let pirateBoatView = UIImageView()
    private let enemyBoatView = UIImageView()
    private let heartView = UIImageView()
    private let lifeCountLabel = UILabel()
    private let lifeCountView = UIImageView()
    private let coinCountLabel = UILabel()
    private let coinCountView = UIImageView()
    private let scoreCountLabel = UILabel()
    private let scoreCountView = UIImageView()
    private let screenWidth = Int(UIScreen.main.bounds.width)
    private let screenHeight = Int(UIScreen.main.bounds.height)
    private let step = 50
    private let enemyBoatSize = 90
    private let lifeCountViewSize = 70
    private let pirateBoatSize = 90
    private let navigationButtonsSize = 70
    
    // MARK: - var
    var currentPostion = (0,0)
    var moveStep: Int = 0
    var enemyArray: [UIImageView] = []
    var coinArray: [UIImageView] = []
    var bulletArray: [UIImageView] = []
    var isAnimated = true
    var coinTimer = Timer()
    var enemyIntersectionTimer = Timer()
    var shoresIntersectionTimer = Timer()
    var boatIntersectionTimer = Timer()
    var bulletIntersectionTimer = Timer()
    var enemyBoatTimer = Timer()
    var mineViewTimer = Timer()
    var pirateViewTimer = Timer()
    var bulletTimer = Timer()
    var lifeViewTimer = Timer()
    var rockViewTimer = Timer()
    var scoreTimer = Timer()
    var motionManager = CMMotionManager()
    var alert = 0
    
    var lifeCount: Int = 0 {
        didSet {
            lifeCountLabel.text = lifeCount.description
        }
    }
    
    var coinCount: Int = 0 {
        didSet {
            coinCountLabel.text = coinCount.description
        }
    }
    
    var scoreCount: Int = 0 {
        didSet {
            scoreCountLabel.text = scoreCount.description
        }
    }
    // MARK: - VC life Func
    override func viewDidLoad() {
        super.viewDidLoad()
        self.createMainViews()
        self.startTimerToMoveViews()
        self.checkViewIntersections()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.selectBoatContols()
    }
    
    // MARK: - IBAction
    @IBAction func moveLeft(_ sender: UIButton) {
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveLinear) {
            self.moveBoat(moveDicerction: .left)
        }
    }
    
    @IBAction func moveRight(_ sender: UIButton) {
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveLinear) {
            self.moveBoat(moveDicerction: .right)
        }
    }
    
    // MARK: - FLow func
    private func selectBoatContols() {
        guard let selectedControl = SettingsManager.shared.loadSettings().selectedControl else {
            return
        }
        if selectedControl == "Tilt" {
            self.setAccelerometerDirectionControl()
            self.makeBoatJump()
        } else {
            self.createBoatLeftDirectionControl()
            self.createBoatRightDirectionControl()
        }
    }
    
    private func defineMoveStep() {
        guard let selectedControl = SettingsManager.shared.loadSettings().selectedControl else {
            return
        }
        if selectedControl == "Tilt" {
            self.moveStep = 5
        } else {
            self.moveStep = 30
        }
    }
    
    private func moveBoat (moveDicerction: MoveDirection) {
        self.defineMoveStep()
        switch moveDicerction {
        case.left:
            self.boatView.frame.origin.x -= CGFloat(self.moveStep)
        case.right:
            self.boatView.frame.origin.x += CGFloat(self.moveStep)
            
        }
    }
    
    private func setAccelerometerDirectionControl() {
        let accelerometerInterval = 0.02
        let animationDuration = 0.3
        let accelerationLimit = 0.2
        if motionManager.isAccelerometerAvailable {
            motionManager.accelerometerUpdateInterval = accelerometerInterval
            motionManager.startAccelerometerUpdates(to: .main) { [weak self] (data: CMAccelerometerData?, error: Error?) in
                if let acceleration = data?.acceleration {
                    if acceleration.x > accelerationLimit {
                        UIView.animate(withDuration: animationDuration, delay: 0, options: .curveLinear) {
                            self?.moveBoat(moveDicerction: .right)
                        }
                    }  else if acceleration.x < -accelerationLimit {
                        UIView.animate(withDuration: animationDuration, delay: 0, options: .curveLinear) {
                            self?.moveBoat(moveDicerction: .left)
                        }
                    }
                }
            }
        }
    }
    
    private func makeBoatJump() {
        let gyroInterval = 0.01
        let gyroLimit = 2.0
        if motionManager.isDeviceMotionAvailable {
            motionManager.gyroUpdateInterval = gyroInterval
            motionManager.startGyroUpdates(to: .main) { [weak self]  (data, error) in
                if let rate = data?.rotationRate {
                    if rate.x > gyroLimit {
                        self?.animateBoatInJump()
                    }
                }
            }
        }
    }
    
    private func animateBoatInJump() {
        let changedBoatSize = CGFloat(3)
        UIView.animate(withDuration: 1.0, delay: 0.0, options: .curveLinear) {
            self.enemyIntersectionTimer.invalidate()
            self.shoresIntersectionTimer.invalidate()
            self.boatIntersectionTimer.invalidate()
            self.bulletIntersectionTimer.invalidate()
            self.boatView.frame.size.width += changedBoatSize
            self.boatView.frame.size.height += changedBoatSize
        } completion: { (_) in
            self.boatView.frame.size.width -= changedBoatSize
            self.boatView.frame.size.height -= changedBoatSize
            self.checkBoatIntersectsEnemies()
            self.checkBoatIntersectsShores()
            self.checkBoatIntersectsEnemyBoat()
            self.checkBulletIntersectsBoat()
        }
    }
    
    private func createBoatLeftDirectionControl() {
        let leftButton = UIButton()
        leftButton.frame = CGRect(x: self.currentPostion.0 + 2*self.step, y: self.screenHeight - self.navigationButtonsSize , width: self.navigationButtonsSize, height: self.navigationButtonsSize)
        leftButton.setImage(UIImage(named:"leftarrow"), for: .normal)
        leftButton.backgroundColor = .none
        leftButton.alpha = 0.7
        leftButton.addTarget(self, action: #selector(moveLeft(_:)), for:.touchUpInside)
        self.view.addSubview(leftButton)
    }
    
    private func createBoatRightDirectionControl() {
        let rightButton = UIButton()
        rightButton.frame = CGRect(x: self.screenWidth - 3*self.step, y: self.screenHeight - self.navigationButtonsSize , width: self.navigationButtonsSize, height: self.navigationButtonsSize)
        rightButton.setImage(UIImage(named:"rightarrow"), for: .normal)
        rightButton.backgroundColor = .none
        rightButton.alpha = 0.7
        rightButton.addTarget(self, action: #selector(moveRight(_:)), for:.touchUpInside)
        self.view.addSubview(rightButton)
    }
    
    private func createFirstBackgroundView() {
        let firstBackgroundView = UIImageView()
        firstBackgroundView.frame = CGRect(x: self.currentPostion.0, y: self.currentPostion.1, width: self.screenWidth, height: self.screenHeight)
        firstBackgroundView.image = UIImage(named: "water")
        firstBackgroundView.contentMode = .scaleToFill
        self.view.insertSubview(firstBackgroundView, belowSubview: backGroundView)
        self.animateBackgroundView(view: firstBackgroundView)
    }
    
    private func createSecondBackgroundView() {
        let secondBackgroundView = UIImageView()
        secondBackgroundView.frame = CGRect(x: self.currentPostion.0, y: -self.screenHeight, width: self.screenWidth, height: self.screenHeight)
        secondBackgroundView.image = UIImage(named: "water")
        secondBackgroundView.contentMode = .scaleToFill
        self.view.insertSubview(secondBackgroundView, belowSubview: backGroundView)
        self.animateBackgroundView(view: secondBackgroundView)
    }
    
    private func animateBackgroundView(view: UIView) {
        if let duration = SettingsManager.shared.loadSettings().selectedLevel {
            if isAnimated {
                UIView.animate(withDuration: duration, delay: 0, options: [.curveLinear, .repeat]) {
                    view.frame.origin.y += CGFloat(self.screenHeight)
                } completion: { (_) in
                    if view.frame.origin.y < CGFloat(self.screenHeight) {
                        self.animateBackgroundView(view: view)
                    } else {
                        self.createSecondBackgroundView()
                        view.removeFromSuperview()
                    }
                }
            }
        }
    }
    
    private func createLesftShoreView() {
        let lesftShore = UIImageView()
        lesftShore.frame = CGRect(x: self.currentPostion.0, y: self.currentPostion.1, width: Int(self.leftShoreView.frame.width), height: self.screenHeight)
        lesftShore.image = UIImage(named: "leftshore")
        lesftShore.contentMode = .scaleToFill
        self.view.insertSubview(lesftShore, aboveSubview: backGroundView)
        self.animateLeftShoreView (view: lesftShore)
    }
    
    private func createSecondLesftShoreView() {
        let secondLeftShore = UIImageView()
        secondLeftShore.frame = CGRect(x: self.currentPostion.0, y: -self.screenHeight, width: Int(self.leftShoreView.frame.width), height: self.screenHeight)
        secondLeftShore.image = UIImage(named: "leftshore")
        secondLeftShore.contentMode = .scaleToFill
        self.view.insertSubview(secondLeftShore, aboveSubview: backGroundView)
        self.animateLeftShoreView (view: secondLeftShore)
    }
    
    private func animateLeftShoreView(view: UIView) {
        if let duration = SettingsManager.shared.loadSettings().selectedLevel {
            if isAnimated {
                UIView.animate(withDuration: duration, delay: 0, options: [.curveLinear, .repeat]) {
                    view.frame.origin.y += CGFloat(self.screenHeight)
                } completion: { (_) in
                    if view.frame.origin.y < CGFloat(self.screenHeight) {
                        self.animateLeftShoreView(view: view)
                    } else {
                        self.createSecondLesftShoreView()
                        view.removeFromSuperview()
                    }
                }
            }
        }
    }
    
    private func createRightShoreView() {
        let rightShore = UIImageView()
        rightShore.frame = CGRect(x: CGFloat(self.screenWidth) - rightShoreView.frame.width, y: self.rightShoreView.frame.origin.y, width: self.rightShoreView.frame.width, height: CGFloat(self.screenHeight))
        rightShore.image = UIImage(named: "rightshore")
        rightShore.contentMode = .scaleToFill
        self.view.insertSubview(rightShore, aboveSubview: backGroundView)
        self.animateRightShoreView(view: rightShore)
    }
    
    private func createSecondRightShoreView() {
        let secondRightShore = UIImageView()
        secondRightShore.frame = CGRect(x: CGFloat(self.screenWidth) - rightShoreView.frame.width, y: -(CGFloat(screenHeight)), width: self.rightShoreView.frame.width, height: CGFloat(self.screenHeight))
        secondRightShore.image = UIImage(named: "rightshore")
        secondRightShore.contentMode = .scaleToFill
        self.view.insertSubview(secondRightShore, aboveSubview: backGroundView)
        self.animateRightShoreView(view: secondRightShore)
    }
    
    private func animateRightShoreView(view: UIView) {
        if let duration = SettingsManager.shared.loadSettings().selectedLevel {
            if isAnimated {
                UIView.animate(withDuration: duration, delay: 0, options: [.curveLinear, .repeat]) {
                    view.frame.origin.y += CGFloat(self.screenHeight)
                } completion: { (_) in
                    if view.frame.origin.y < CGFloat(self.screenHeight) {
                        self.animateRightShoreView(view: view)
                    } else {
                        self.createSecondRightShoreView()
                        view.removeFromSuperview()
                    }
                }
            }
        }
    }
    
    private func createBoatView() {
        let boatSize = 35
        guard let selectedBoat = SettingsManager.shared.loadSettings().selectedboat else { return }
        self.boatView.frame = CGRect(x: self.screenWidth/2 - boatSize/2, y: self.screenHeight - 135, width: boatSize, height: boatSize)
        self.boatView.image = UIImage(named: selectedBoat)
        self.boatView.contentMode = .scaleAspectFill
        self.view.insertSubview(boatView, aboveSubview: backGroundView)
    }
    
    private func createEnemyBoatView() {
        self.enemyBoatView.frame = CGRect(x: self.screenWidth - 3*self.step, y: self.screenHeight/2 - self.step, width: self.enemyBoatSize, height: self.enemyBoatSize)
        self.enemyBoatView.image = UIImage(named: "enemyboat")
        self.enemyBoatView.contentMode = .scaleAspectFill
        self.view.insertSubview(enemyBoatView, aboveSubview: backGroundView)
        self.animateEnemyBoat(enemyBoatToAnimate: enemyBoatView)
    }
    
    private func animateEnemyBoat(enemyBoatToAnimate: UIView) {
        if let duration = SettingsManager.shared.loadSettings().selectedLevel {
            if isAnimated {
                UIView.animate(withDuration: duration/16, delay: 0, options: .curveLinear) {
                    enemyBoatToAnimate.frame.origin.y += 2*CGFloat(self.step)
                    enemyBoatToAnimate.frame.origin.x -= CGFloat(self.step)
                } completion: { (_) in
                    if enemyBoatToAnimate.frame.origin.y > CGFloat(self.screenHeight) {
                        enemyBoatToAnimate.removeFromSuperview()
                    } else {
                        self.animateEnemyBoat(enemyBoatToAnimate: enemyBoatToAnimate)
                    }
                }
            }
        }
    }
    
    private func createPirateBoatView() {
        let x = defineRandomX(viewToDefineX: pirateBoatView)
        self.pirateBoatView.frame = CGRect(x: x, y: -self.step, width: self.pirateBoatSize, height: self.pirateBoatSize)
        self.pirateBoatView.image = UIImage(named: "pirateboat")
        self.pirateBoatView.contentMode = .scaleAspectFill
        self.view.insertSubview(self.pirateBoatView, aboveSubview: self.backGroundView)
        self.animateView(viewToAnimate: self.pirateBoatView)
        self.enemyArray.append(self.pirateBoatView)
    }
    
    private func addPirateBoatBullet() {
        let interval = 0.3
        let bulletSize = 80
        self.bulletTimer = Timer.scheduledTimer(withTimeInterval: TimeInterval(interval), repeats: true, block: { (timer) in
            if let pirateBoatFrame = self.pirateBoatView.layer.presentation()?.frame {
                let bulletView = UIImageView()
                bulletView.frame = CGRect(x: Int(pirateBoatFrame.maxX) - self.pirateBoatSize/2 - bulletSize/2, y: Int(pirateBoatFrame.maxY) - self.pirateBoatSize/2, width: bulletSize, height: bulletSize)
                bulletView.image = UIImage(named: "bullet")
                bulletView.contentMode = .scaleAspectFill
                self.view.insertSubview(bulletView, aboveSubview: self.backGroundView)
                self.bulletArray.append(bulletView)
                self.animateBulletView(viewToAnimate: bulletView)
                self.enemyArray.append(bulletView)
            }
        })
    }
    
    private func animateBulletView (viewToAnimate: UIView) {
        if let duration = SettingsManager.shared.loadSettings().selectedLevel {
            if isAnimated {
                UIView.animate(withDuration: duration/2, delay: 0, options: .curveLinear) {
                    viewToAnimate.frame.origin.y += CGFloat(self.screenHeight)
                } completion: { (_) in
                    if viewToAnimate.frame.origin.y > CGFloat(self.screenHeight) {
                        viewToAnimate.removeFromSuperview()
                    } else {
                        self.animateView(viewToAnimate: viewToAnimate)
                    }
                }
            }
        }
    }
    
    private func createRockView() {
        let rockView = UIImageView()
        let rockSize = 70
        let x = defineRandomX(viewToDefineX: rockView)
        rockView.frame = CGRect(x: x, y: -self.step, width: rockSize, height: rockSize)
        rockView.image = UIImage(named: "rock")
        rockView.contentMode = .scaleToFill
        self.view.insertSubview(rockView, aboveSubview: backGroundView)
        self.animateView(viewToAnimate: rockView)
        self.enemyArray.append(rockView)
    }
    
    private func createMineView() {
        let mineView = UIImageView()
        let mineSize = 35
        let x = defineRandomX(viewToDefineX: mineView)
        mineView.frame = CGRect(x: x, y: -self.step, width: mineSize, height: mineSize)
        mineView.image = UIImage(named: "mine")
        mineView.contentMode = .scaleAspectFill
        self.view.insertSubview(mineView, aboveSubview: backGroundView)
        self.animateView(viewToAnimate: mineView)
        self.enemyArray.append(mineView)
    }
    
    private func createSplashView() {
        let splashView = UIImageView()
        let splashSize = 50
        splashView.frame = CGRect(x: Int(self.enemyBoatView.frame.maxX) - self.enemyBoatSize/2, y: Int(self.enemyBoatView.frame.maxY) - self.enemyBoatSize/2, width: splashSize, height: splashSize)
        splashView.image = UIImage(named: "watersplash")
        splashView.contentMode = .scaleAspectFill
        self.view.insertSubview(splashView, aboveSubview: self.backGroundView)
        self.animateView(viewToAnimate: splashView)
    }
    
    private func createHeartView() {
        let heartSize = 40
        let x = defineRandomX(viewToDefineX: self.heartView)
        self.heartView.frame = CGRect(x: x, y: -self.step, width: heartSize, height: heartSize)
        self.heartView.image = UIImage(named: "life")
        self.heartView.contentMode = .scaleAspectFill
        self.view.insertSubview(self.heartView, aboveSubview: self.backGroundView)
        self.animateView(viewToAnimate: self.heartView)
    }
    
    private func createCoinView() {
        let interval = 5.6
        let coinSize = 30
        self.coinTimer = Timer.scheduledTimer(withTimeInterval: TimeInterval(interval), repeats: true, block: { (timer) in
            let coinView = UIImageView()
            let x = self.defineRandomX(viewToDefineX: coinView)
            coinView.frame = CGRect(x: x, y: -self.step, width: coinSize, height: coinSize)
            coinView.image = UIImage(named: "coin")
            coinView.contentMode = .scaleAspectFill
            self.view.insertSubview(coinView, aboveSubview: self.backGroundView)
            self.animateView(viewToAnimate: coinView)
            self.coinArray.append(coinView)
        })
    }
    
    private func createMainViews() {
        self.createFirstBackgroundView()
        self.createSecondBackgroundView()
        self.createLesftShoreView()
        self.createSecondLesftShoreView()
        self.createRightShoreView()
        self.createSecondRightShoreView()
        self.createBoatView()
        self.createCoinView()
        self.addPirateBoatBullet()
        self.addLifeCounterView()
        self.countLife()
        self.addCoinCounterView()
        self.countCoin()
        self.addScoreCounterView()
        self.countScore()
    }
    
    private func addLifeCounterView() {
        self.lifeCountView.frame = CGRect(x: Int(self.backGroundView.frame.origin.x), y: self.step, width: self.lifeCountViewSize, height: self.lifeCountViewSize)
        self.lifeCountView.layer.borderWidth = 2
        self.lifeCountView.layer.borderColor = UIColor.white.cgColor
        self.lifeCountView.layer.cornerRadius = 10
        self.view.addSubview(lifeCountView)
        self.addLifeCountLabel()
        self.addlifeView()
    }
    
    private func addLifeCountLabel() {
        self.lifeCountLabel.frame = CGRect(x: self.lifeCountView.frame.width/3, y: 0, width: self.lifeCountView.frame.width/2, height: self.lifeCountView.frame.height)
        self.lifeCountLabel.text = lifeCount.description
        self.lifeCountLabel.textAlignment = .right
        self.lifeCountLabel.textColor = .white
        self.lifeCountView.addSubview(self.lifeCountLabel)
    }
    
    private func addlifeView() {
        let lifeView = UIImageView()
        let xPoint: CGFloat = 10
        lifeView.frame = CGRect(x: xPoint, y: self.lifeCountView.frame.height/4, width: self.lifeCountView.frame.width/3, height: self.lifeCountView.frame.height/2)
        lifeView.image = UIImage(named: "life")
        lifeView.contentMode = .scaleAspectFill
        self.lifeCountView.addSubview(lifeView)
    }
    
    private func addCoinCounterView() {
        let screenOffset = 130
        self.coinCountView.frame = CGRect(x: screenWidth - screenOffset , y: self.step, width: self.lifeCountViewSize, height: self.lifeCountViewSize)
        self.coinCountView.layer.borderWidth = 2
        self.coinCountView.layer.borderColor = UIColor.white.cgColor
        self.coinCountView.layer.cornerRadius = 10
        self.view.addSubview(coinCountView)
        self.addCoinCountLabel()
        self.addCoinView()
    }
    
    private func addCoinCountLabel() {
        self.coinCountLabel.frame = CGRect(x: self.coinCountView.frame.width/3, y: 0, width: self.coinCountView.frame.width/2, height: self.coinCountView.frame.height)
        self.coinCountLabel.text = coinCount.description
        self.coinCountLabel.textAlignment = .right
        self.coinCountLabel.textColor = .white
        self.coinCountView.addSubview(coinCountLabel)
    }
    
    private func addCoinView() {
        let coinView = UIImageView()
        coinView.frame = CGRect(x: 10, y: self.coinCountView.frame.height/3, width: self.coinCountView.frame.width/4, height: self.coinCountView.frame.height/3)
        coinView.image = UIImage(named: "coin")
        coinView.contentMode = .scaleAspectFill
        self.coinCountView.addSubview(coinView)
    }
    
    private func addScoreCounterView() {
        let screenOffset = 130
        self.scoreCountView.frame = CGRect(x: screenWidth - screenOffset , y: 3*self.step, width: self.lifeCountViewSize, height: self.lifeCountViewSize)
        self.scoreCountView.layer.borderWidth = 2
        self.scoreCountView.layer.borderColor = UIColor.white.cgColor
        self.scoreCountView.layer.cornerRadius = 10
        self.view.addSubview(self.scoreCountView)
        self.addScoreCountLabel()
        self.addScoreView()
    }
    
    private func addScoreCountLabel() {
        self.scoreCountLabel.frame = CGRect(x: self.scoreCountView.frame.width/3, y: 0, width: self.scoreCountView.frame.width/2, height: self.scoreCountView.frame.height)
        self.scoreCountLabel.text = scoreCount.description
        self.scoreCountLabel.textAlignment = .right
        self.scoreCountLabel.textColor = .white
        self.scoreCountView.addSubview(self.scoreCountLabel)
    }
    
    private func addScoreView() {
        let scoreView = UIImageView()
        let xPoint: CGFloat = 10
        scoreView.frame = CGRect(x: xPoint, y: self.scoreCountView.frame.height/3, width: self.scoreCountView.frame.width/4, height: self.scoreCountView.frame.height/2)
        scoreView.image = UIImage(named: "finish")
        scoreView.contentMode = .scaleAspectFill
        self.scoreCountView.addSubview(scoreView)
    }
    
    private func countLife() {
        let interval = 0.01
        let timer = Timer.scheduledTimer(withTimeInterval: interval, repeats: true) { (internalTimer) in
            if let frame = self.heartView.layer.presentation()?.frame {
                if self.boatView.frame.intersects(frame) {
                    self.heartView.removeFromSuperview()
                    self.lifeCount += 1
                }
            }
        }
    }
    
    private func countCoin() {
        let interval = 0.01
        let timer = Timer.scheduledTimer(withTimeInterval: interval, repeats: true) { (internalTimer) in
            for coin in self.coinArray {
                if let frame = coin.layer.presentation()?.frame {
                    if self.boatView.frame.intersects(frame) {
                        coin.removeFromSuperview()
                        self.coinCount += 1
                    }
                }
            }
        }
    }
    
    private func countScore() {
        let interval = 1.0
        scoreTimer = Timer.scheduledTimer(withTimeInterval: interval, repeats: true) { (internalTimer) in
            self.scoreCount += 1
        }
    }
    
    private func defineRandomX (viewToDefineX: UIView) -> Int {
        let myViewWidth = viewToDefineX.frame.width
        let widthX = CGFloat(self.backGroundView.frame.width) - 2*myViewWidth
        let randowWidth = CGFloat.random(in: self.backGroundView.frame.origin.x...widthX)
        viewToDefineX.center.x = randowWidth
        return Int(viewToDefineX.center.x)
    }
    
    private func animateView (viewToAnimate: UIView) {
        if let duration = SettingsManager.shared.loadSettings().selectedLevel {
            if isAnimated {
                UIView.animate(withDuration: duration, delay: 0, options: .curveLinear) {
                    viewToAnimate.frame.origin.y += CGFloat(self.screenHeight)
                } completion: { (_) in
                    if viewToAnimate.frame.origin.y > CGFloat(self.screenHeight) {
                        viewToAnimate.removeFromSuperview()
                    } else {
                        self.animateView(viewToAnimate: viewToAnimate)
                    }
                }
            }
        }
    }
    
    private func startTimerForRockView() {
        let interval = 5.0
        self.rockViewTimer = Timer.scheduledTimer(withTimeInterval: TimeInterval(interval), repeats: true) { (internalTimer) in
            self.createRockView()
        }
        self.rockViewTimer.fire()
    }
    
    private func startTimerForLifeView() {
        let interval = 23.0
        self.lifeViewTimer = Timer.scheduledTimer(withTimeInterval: TimeInterval(interval), repeats: true) { (internalTimer) in
            self.createHeartView()
        }
    }
    
    private func startTimerForMineView() {
        let interval = 8.0
        self.mineViewTimer = Timer.scheduledTimer(withTimeInterval: TimeInterval(interval), repeats: true) { (internalTimer) in
            self.createMineView()
        }
    }
    
    private func startTimerForPirateView() {
        let interval = 19.0
        self.pirateViewTimer = Timer.scheduledTimer(withTimeInterval: TimeInterval(interval), repeats: true) { (internalTimer) in
            self.createPirateBoatView()
        }
    }
    
    private func startTimerForEnemyBoatView() {
        let interval = 16.8
        self.enemyBoatTimer = Timer.scheduledTimer(withTimeInterval: TimeInterval(interval), repeats: true) { (internalTimer) in
            self.createEnemyBoatView()
        }
    }
    
    private func endGame() {
        if self.lifeCount < 0 {
            isAnimated = false
            self.invalidateTimers()
            self.lifeCountLabel.text = "0"
            self.createGameOverView()
            self.showGameOverAlert()
        }
    }
    
    private func invalidateTimers() {
        self.coinTimer.invalidate()
        self.enemyBoatTimer.invalidate()
        self.pirateViewTimer.invalidate()
        self.mineViewTimer.invalidate()
        self.lifeViewTimer.invalidate()
        self.rockViewTimer.invalidate()
        self.bulletTimer.invalidate()
        self.enemyIntersectionTimer.invalidate()
        self.shoresIntersectionTimer.invalidate()
        self.bulletIntersectionTimer.invalidate()
        self.scoreTimer.invalidate()
    }
    
    private func createGameOverView() {
        let gameOverView = UIImageView()
        gameOverView.frame = CGRect(x: self.currentPostion.0, y: self.currentPostion.1, width: self.screenWidth, height: self.screenHeight)
        gameOverView.image = UIImage(named: "settingsMenu")
        gameOverView.contentMode = .scaleAspectFill
        gameOverView.clipsToBounds = true
        self.view.addSubview(gameOverView)
    }
    
    private func saveGameRelults() {
        let date = Date()
        let formater = DateFormatter()
        formater.dateFormat = "MMM d, h:mm"
        let dateString = formater.string(from: date)
        let currentResult = UserResults(coinCount: self.coinCount, scoreCount: self.scoreCount, date: dateString)
        RecordManager.shared.saveRecords(arrayOfResults: currentResult)
    }
    
    private func showGameOverAlert() {
        if self.alert == 0 {
            if let name = SettingsManager.shared.loadSettings().captainName {
                showAlert(title: "GAME OVER".localized, message: "\(NSLocalizedString("Coins collected by", comment: "")) \(name): \(coinCount)", actionTitles: ["Back to Menu".localized], actionStyle: [.default], actions: [{ (_) in
                    self.navigationController?.popToRootViewController(animated: true)
                    self.saveGameRelults()
                }], vc: self)
            }
        }
        self.alert += 1
    }
    
    private func checkBoatIntersectsEnemies() {
        let interval = 0.1
        self.enemyIntersectionTimer = Timer.scheduledTimer(withTimeInterval: interval, repeats: true) { (internalTimer) in
            for enemy in self.enemyArray {
                if let frame = enemy.layer.presentation()?.frame {
                    if self.boatView.frame.intersects(frame) {
                        self.lifeCount -= 1
                        self.endGame()
                    }
                }
            }
        }
        self.enemyIntersectionTimer.fire()
    }
    
    private func checkEnemyBoatIntersectsEnemies() {
        let interval = 0.1
        let timer = Timer.scheduledTimer(withTimeInterval: interval, repeats: true) { (internalTimer) in
            for enemy in self.enemyArray {
                if let frame = enemy.layer.presentation()?.frame {
                    if self.enemyBoatView.frame.intersects(frame) {
                        self.createSplashView()
                        self.enemyBoatView.removeFromSuperview()
                    }
                }
            }
        }
    }
    
    private func checkBulletIntersectsBoat() {
        let interval = 0.1
        self.bulletIntersectionTimer = Timer.scheduledTimer(withTimeInterval: interval, repeats: true) { (internalTimer) in
            for bullet in self.bulletArray {
                if let frame = bullet.layer.presentation()?.frame {
                    if self.boatView.frame.intersects(frame) {
                        self.lifeCount -= 1
                        self.endGame()
                    }
                }
            }
        }
        self.bulletIntersectionTimer.fire()
    }
    
    private func checkBulletIntersectsEnemyBoat() {
        let interval = 0.1
        let timer = Timer.scheduledTimer(withTimeInterval: interval, repeats: true) { (internalTimer) in
            for bullet in self.bulletArray {
                if let frame = bullet.layer.presentation()?.frame {
                    if self.enemyBoatView.frame.intersects(frame) {
                        self.createSplashView()
                        self.enemyBoatView.removeFromSuperview()
                    }
                }
            }
        }
    }
    
    private func checkBoatIntersectsShores() {
        let interval = 0.1
        self.shoresIntersectionTimer = Timer.scheduledTimer(withTimeInterval: interval, repeats: true) { (internalTimer) in
            if let frame = self.rightShoreView.layer.presentation()?.frame {
                if self.boatView.frame.intersects(frame) {
                    self.lifeCount -= 1
                    self.endGame()
                }
            }
            if let frame = self.leftShoreView.layer.presentation()?.frame {
                if self.boatView.frame.intersects(frame) {
                    self.lifeCount -= 1
                    self.endGame()
                }
            }
        }
        self.shoresIntersectionTimer.fire()
    }
    
    private func checkBoatIntersectsEnemyBoat() {
        let interval = 0.1
        self.boatIntersectionTimer = Timer.scheduledTimer(withTimeInterval: interval, repeats: true) { (internalTimer) in
            if let frame = self.enemyBoatView.layer.presentation()?.frame {
                if self.boatView.frame.intersects(frame) {
                    self.lifeCount -= 1
                    self.endGame()
                }
            }
        }
        self.boatIntersectionTimer.fire()
    }
    
    private func checkCoinsIntersectsEnemies() {
        let interval = 0.1
        let timer = Timer.scheduledTimer(withTimeInterval: interval, repeats: true) { (internalTimer) in
            for coin in self.coinArray {
                if let coinframe = coin.layer.presentation()?.frame {
                    for enemy in self.enemyArray {
                        if let enemyframe = enemy.layer.presentation()?.frame {
                            if coinframe.intersects(enemyframe) {
                                coin.removeFromSuperview()
                            }
                        }
                    }
                }
            }
        }
    }
    
    private func checkViewIntersections() {
        self.checkBoatIntersectsEnemies()
        self.checkBoatIntersectsShores()
        self.checkEnemyBoatIntersectsEnemies()
        self.checkBoatIntersectsEnemyBoat()
        self.checkCoinsIntersectsEnemies()
        self.checkBulletIntersectsEnemyBoat()
        self.checkBulletIntersectsBoat()
    }
    
    private func startTimerToMoveViews() {
        self.startTimerForRockView()
        self.startTimerForEnemyBoatView()
        self.startTimerForMineView()
        self.startTimerForPirateView()
        self.startTimerForLifeView()
    }
}

