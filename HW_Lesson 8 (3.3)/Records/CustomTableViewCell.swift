import UIKit

class CustomTableViewCell: UITableViewCell {
    
    // MARK: - IBOutlets
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var coinsLabel: UILabel!
        
    // MARK: - FLow func
    func configure(object: UserResults) {
        self.dateLabel.text = object.date
        if let score = object.scoreCount {
            self.scoreLabel.text = "\(score)"
        }
        if let numberOfCoins = object.coinCount {
            self.coinsLabel.text = "\(numberOfCoins)"
        }
        
    }
    
}
