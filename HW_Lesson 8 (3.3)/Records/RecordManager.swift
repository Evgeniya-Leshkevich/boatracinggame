import Foundation
import UIKit

enum ResultKeys: String {
    case newKey = "newkey"
}

class RecordManager {
    
    static let shared = RecordManager()
    private init() {}
    
    func saveRecords(arrayOfResults: UserResults) {
        var array = self.loadRecords()
        array.append(arrayOfResults)
        UserDefaults.standard.set(encodable: array, forKey: ResultKeys.newKey.rawValue)
    }
    
    func loadRecords() -> [UserResults] {
        guard let arrayOfResults = UserDefaults.standard.value([UserResults].self, forKey: ResultKeys.newKey.rawValue) else {
            return []
        }
        return arrayOfResults
    }
    
}
