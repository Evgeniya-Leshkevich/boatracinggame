import UIKit

class RecordViewController: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var totalSumOfCoinsLabel: UILabel!
    @IBOutlet weak var coinsLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var bestScoreLabel: UILabel!
    @IBOutlet weak var scoreView: UIView!
    @IBOutlet weak var backToMenuButton: UIButton!
    
    // MARK: - let
    private let arrayOfResults = RecordManager.shared.loadRecords()
    private let highScoreNumber = 10
    
    // MARK: - var
    var highScore: [UserResults] = []
    
    // MARK: - VC life Func
    override func viewDidLoad() {
        super.viewDidLoad()
        self.highScore = arrayOfResults.sorted(by: {$0.scoreCount ?? 0 > $1.scoreCount ?? 0})
        self.scoreView.layer.cornerRadius = 30
        self.loadResultsOfGame()
        self.configureLabels()
    }
    
    // MARK: - IBAction
    @IBAction func backToMenu(_ sender: UIButton) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    // MARK: - FLow func
    private func loadResultsOfGame() {
        let sumOfCoins = self.arrayOfResults.reduce(0, {$0 + ($1.coinCount ?? 0)})
        self.totalSumOfCoinsLabel.text = "\(NSLocalizedString("Total sum of coins collected:", comment: "")) \(sumOfCoins)"
    }
    
    private func configureLabels() {
        let bestScoreLabelText = "Best scores".localized
        self.bestScoreLabel.font = UIFont(name: "Nunito-ExtraBold", size: 32)
        self.bestScoreLabel.text = bestScoreLabelText
        
        let dateLabelText = "Date".localized
        self.dateLabel.font = UIFont(name: "Nunito-ExtraBold", size: 24)
        self.dateLabel.text = dateLabelText
        
        let scoreLabelText = "Score".localized
        self.scoreLabel.font = UIFont(name: "Nunito-ExtraBold", size: 24)
        self.scoreLabel.text = scoreLabelText
        
        let coinsLabelText = "Coins".localized
        self.coinsLabel.font = UIFont(name: "Nunito-ExtraBold", size: 24)
        self.coinsLabel.text = coinsLabelText
        
        self.backToMenuButton.setTitle("Back to Menu".localized, for: .normal)
    }
}

// MARK: - extension
extension RecordViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayOfResults.prefix(self.highScoreNumber).count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CustomTableViewCell", for: indexPath) as? CustomTableViewCell
        else {
            return UITableViewCell()
        }
        cell.configure(object: self.highScore[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}


