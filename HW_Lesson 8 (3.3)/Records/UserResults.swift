import Foundation

class UserResults: Codable {
    var coinCount: Int?
    var scoreCount: Int?
    var date: String?
    
    init(coinCount: Int?, scoreCount: Int?, date: String?) {
        self.coinCount = coinCount
        self.scoreCount = scoreCount
        self.date = date
    }
    
    private enum CodingKeys: String, CodingKey {
        case coinCount
        case scoreCount
        case date
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        coinCount = try container.decodeIfPresent(Int.self, forKey: .coinCount)
        scoreCount = try container.decodeIfPresent(Int.self, forKey: .scoreCount)
        date = try container.decodeIfPresent(String.self, forKey: .date)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(self.coinCount, forKey: .coinCount)
        try container.encode(self.scoreCount, forKey: .scoreCount)
        try container.encode(self.date, forKey: .date)
    }
    
}
