import Foundation
import UIKit

enum Keys: String {
    case firstKey = "key"
}

class SettingsManager {
    
    static let shared = SettingsManager()
    private init() {}
    
    func saveSettings(userSettings: UserSettings) {
        UserDefaults.standard.set(encodable: userSettings, forKey: Keys.firstKey.rawValue)
    }
    
    func loadSettings() -> UserSettings {
        guard let userSettings = UserDefaults.standard.value(UserSettings.self, forKey: Keys.firstKey.rawValue) else {
            return UserSettings(captainName: "",selectedBoat: "boat",selectedLevel: 5.0, selectedControl: "Tilt")
        }
        return userSettings
    }
    
}
