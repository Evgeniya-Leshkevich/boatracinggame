import UIKit

class SettingsViewController: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var captainNameText: UITextField!
    @IBOutlet weak var buttonView: UIView!
    @IBOutlet weak var firstBoatButton: UIButton!
    @IBOutlet weak var secondBoatButton: UIButton!
    @IBOutlet weak var thirdBoatButton: UIButton!
    @IBOutlet weak var easyLevelButton: UIButton!
    @IBOutlet weak var mediumLevelButton: UIButton!
    @IBOutlet weak var hardLevelButton: UIButton!
    @IBOutlet weak var backToMenuButton: UIButton!
    @IBOutlet weak var settingsLabel: UILabel!
    @IBOutlet weak var captainNameLabel: UILabel!
    @IBOutlet weak var boatSelectionLabel: UILabel!
    @IBOutlet weak var levelSelectionLabel: UILabel!
    @IBOutlet weak var controlSelectionLabel: UILabel!
    @IBOutlet var boatButtons: [UIButton]!
    @IBOutlet var levelButtons: [UIButton]!
    @IBOutlet weak var navigationControl: UISegmentedControl!
    
    // MARK: - var
    var selectedBoat = ""
    var captainName = ""
    var selectedLevel = 0.0
    var selectedControl = ""
    var level = ""
    
    // MARK: - VC life Func
    override func viewDidLoad() {
        super.viewDidLoad()
        self.createBoatButtons()
        self.loadSettings()
        self.localizeText()
    }
    
    // MARK: - IBAction
    @IBAction func backToMenuButtonPressed(_ sender: UIButton) {
        self.navigationController?.popToRootViewController(animated: true)
        self.saveButtonPressed()
    }
    
    @IBAction func boatButtonIsSelected(_ sender: UIButton) {
        for element in boatButtons {
            element.isSelected  = false
        }
        sender.isSelected = !sender.isSelected
        self.configueBoatButtons()
    }
    
    @IBAction func levelButtonIsSelected(_ sender: UIButton) {
        for element in levelButtons {
            element.isSelected  = false
        }
        sender.isSelected = !sender.isSelected
        self.configueLevelButtons()
    }
    
    @IBAction func didChangedSegment(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            self.selectedControl = "Tilt"
        } else {
            self.selectedControl = "Buttons"
        }
    }
    
    
    // MARK: - FLow func
    private func saveButtonPressed() {
        if let captainName = captainNameText.text {
            let userSettings = UserSettings(captainName: captainName,  selectedBoat: self.selectedBoat, selectedLevel: self.selectedLevel, selectedControl: self.selectedControl)
            SettingsManager.shared.saveSettings(userSettings: userSettings)
        }
    }
    
    private func createFirstBoatButton() {
        self.firstBoatButton.setImage(UIImage(named:"boat"), for: .normal)
        self.firstBoatButton.titleLabel?.text = "boat"
        self.buttonView.addSubview(self.firstBoatButton)
    }
    
    private func createSecondBoatButton() {
        self.secondBoatButton.setImage(UIImage(named:"secondboat"), for: .normal)
        self.secondBoatButton.titleLabel?.text = "secondboat"
        self.buttonView.addSubview(self.secondBoatButton)
    }
    
    private func createThirdBoatButton() {
        self.thirdBoatButton.setImage(UIImage(named:"thirdboat"), for: .normal)
        self.thirdBoatButton.titleLabel?.text = "thirdboat"
        self.buttonView.addSubview(self.thirdBoatButton)
    }
    
    private func configureLevelButtons() {
        for element in levelButtons {
            element.layer.borderWidth = 4
            element.layer.borderColor = UIColor.white.cgColor
            element.layer.cornerRadius = 30
        }
    }
    
    private func configueBoatButtons() {
        for element in boatButtons {
            if element.isSelected {
                element.layer.borderColor = UIColor.green.cgColor
                element.layer.borderWidth = 4
                element.layer.cornerRadius = 30
                if let selectedBoat = element.titleLabel?.text {
                    self.selectedBoat = selectedBoat
                }
            } else {
                element.layer.borderColor = nil
                element.layer.borderWidth = 0
            }
        }
    }
    
    private func configueLevelButtons() {
        for element in levelButtons {
            if element.isSelected {
                element.layer.borderColor = UIColor.green.cgColor
                if let level = element.titleLabel?.text?.localized {
                    self.level = level
                }
                self.defineSelectedLevel()
            } else {
                element.layer.borderColor = UIColor.white.cgColor
            }
        }
    }
    
    private func defineSelectedLevel() {
        switch level {
        case "Easy".localized:
            self.selectedLevel = 4.0
        case "Medium".localized:
            self.selectedLevel = 3.0
        case "Hard".localized:
            self.selectedLevel = 2.0
        default:
            self.selectedLevel = 4.0
        }
    }
    
    private func loadSettings() {
        self.loadCaptainName()
        self.loadSelectedBoat()
        self.loadSelectedLevel()
        self.loadSelectedControl()
    }
    
    private func loadCaptainName() {
        if let name = SettingsManager.shared.loadSettings().captainName {
            self.captainNameText.text = name
        }
    }
    
    private func loadSelectedBoat() {
        if let selectedBoat = SettingsManager.shared.loadSettings().selectedboat {
            for element in boatButtons {
                if element.titleLabel?.text == "\(selectedBoat)" {
                    self.boatButtonIsSelected(element)
                }
            }
        }
    }
    
    private  func loadSelectedLevel() {
        if let selectedLevel = SettingsManager.shared.loadSettings().selectedLevel {
            switch selectedLevel {
            case 4.0:
                self.levelButtonIsSelected(self.easyLevelButton)
            case 3.0:
                self.levelButtonIsSelected(self.mediumLevelButton)
            case 2.0:
                self.levelButtonIsSelected(self.hardLevelButton)
            default:
                self.levelButtonIsSelected(self.easyLevelButton)
            }
        }
    }
    
    private func loadSelectedControl() {
        if let selectedControl = SettingsManager.shared.loadSettings().selectedControl {
            switch selectedControl {
            case "Tilt":
                self.navigationControl.selectedSegmentIndex = 0
            default:
                self.navigationControl.selectedSegmentIndex = 1
            }
        }
    }
    
    private func localizeText() {
        self.backToMenuButton.setTitle("Back to Menu".localized, for: .normal)
        self.easyLevelButton.setTitle("Easy".localized, for: .normal)
        self.mediumLevelButton.setTitle("Medium".localized, for: .normal)
        self.hardLevelButton.setTitle("Hard".localized, for: .normal)
        self.navigationControl.setTitle("Tilt".localized, forSegmentAt: 0)
        self.navigationControl.setTitle("Buttons".localized, forSegmentAt: 1)
        self.settingsLabel.text = "Settings".localized
        self.captainNameLabel.text = "Captain name".localized
        self.boatSelectionLabel.text = "Select your boat".localized
        self.levelSelectionLabel.text = "Select difficulty level".localized
        self.controlSelectionLabel.text = "Select your controls".localized
    }
    
    private func createBoatButtons() {
        self.createFirstBoatButton()
        self.createSecondBoatButton()
        self.createThirdBoatButton()
        self.configureLevelButtons()
    }
}

// MARK: - extension
extension SettingsViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
