import Foundation

class UserSettings: Codable {
    var captainName: String?
    var selectedboat: String?
    var selectedLevel: Double?
    var selectedControl: String?
    
    init(captainName: String?, selectedBoat: String?, selectedLevel: Double?, selectedControl: String?) {
        self.captainName = captainName
        self.selectedboat = selectedBoat
        self.selectedLevel = selectedLevel
        self.selectedControl = selectedControl
    }
    
    private enum CodingKeys: String, CodingKey {
        case captainName
        case selectedboat
        case selectedLevel
        case selectedControl
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        captainName = try container.decodeIfPresent(String.self, forKey: .captainName)
        selectedboat = try container.decodeIfPresent(String.self, forKey: .selectedboat)
        selectedLevel = try container.decodeIfPresent(Double.self, forKey: .selectedLevel)
        selectedControl = try container.decodeIfPresent(String.self, forKey: .selectedControl)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(self.captainName, forKey: .captainName)
        try container.encode(self.selectedboat, forKey: .selectedboat)
        try container.encode(self.selectedLevel, forKey: .selectedLevel)
        try container.encode(self.selectedControl, forKey: .selectedControl)
    }
    
}






