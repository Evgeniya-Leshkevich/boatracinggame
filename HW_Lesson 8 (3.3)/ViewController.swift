import UIKit

class ViewController: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet var navigationButtons: [UIButton]!
    @IBOutlet weak var newGameLabel: UILabel!
    @IBOutlet weak var recordsLabel: UILabel!
    @IBOutlet weak var settingsLabel: UILabel!
    
    // MARK: - VC life Func
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureButtons()
        self.createLabels()
    }
    
    // MARK: - IBAction
    @IBAction func goToGameViewController(_ sender: UIButton) {
        guard let controller = self.storyboard?.instantiateViewController(withIdentifier: "GameViewController") as? GameViewController else {
            return
        }
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    
    @IBAction func goToRecordViewController(_ sender: UIButton) {
        guard let controller = self.storyboard?.instantiateViewController(withIdentifier: "RecordViewController") as? RecordViewController else {
            return
            
        }
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func goToSettingsViewController(_ sender: UIButton) {
        guard let controller = self.storyboard?.instantiateViewController(withIdentifier: "SettingsViewController") as? SettingsViewController else {
            return
        }
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    // MARK: - VC life Func
    private func configureButtons() {
        for element in navigationButtons {
            element.layer.borderWidth = 4
            element.layer.borderColor = UIColor.white.cgColor
            element.dropShadow()
        }
    }
    
    private func createLabels() {
        let myShadow = NSShadow()
        myShadow.shadowBlurRadius = 0
        myShadow.shadowOffset = CGSize(width: 0, height: 2)
        myShadow.shadowColor = UIColor.black.withAlphaComponent(0.2)
        let myAttribute = [NSAttributedString.Key.shadow: myShadow]
        
        let gameText = "NEW GAME".localized
        self.newGameLabel.font = UIFont(name: "Nunito-ExtraBold", size: 24)
        let gameAttrString = NSAttributedString(string: gameText, attributes: myAttribute)
        self.newGameLabel.attributedText = gameAttrString
        
        let recordsText = "RECORDS".localized
        self.recordsLabel.font = UIFont(name: "Nunito-ExtraBold", size: 24)
        let recordsAttrString = NSAttributedString(string: recordsText, attributes: myAttribute)
        self.recordsLabel.attributedText = recordsAttrString
        
        let settingsText = "SETTINGS".localized
        self.settingsLabel.font = UIFont(name: "Nunito-ExtraBold", size: 24)
        let settingsAttrString = NSAttributedString(string: settingsText, attributes: myAttribute)
        self.settingsLabel.attributedText = settingsAttrString
    }
}

